
package serverpackage;

import java.util.Queue;
import java.util.LinkedList;

public class GameQueue implements Runnable {
    private final FrmServer server;
    private final Queue<Player> queue;
    private boolean keepRunning;
    
    public GameQueue(FrmServer server) {
        this.server = server;
        queue = new LinkedList<Player>();
    }
    
    @Override
    public void run() {
        System.out.println("Starting the Queue..."); //////////////
        keepRunning = true;
        while(keepRunning) {
            synchronized (queue) {
                if (queue.size() >= 2) {
                    Player playerX = queue.poll();
                    Player playerO = queue.poll();
                    Game game = new Game(playerX);
                    playerX.setupGame(game, playerO, 'X');
                    playerO.setupGame(game, playerX, 'O');
                    //Avisar o Server
                } else {
                    try {
                        queue.wait();
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
    }
    
    public void joinQueue(Player p) {
        synchronized (queue) {
            if (!queue.contains(p)) {
                queue.offer(p);
                queue.notify();
                System.out.println("Queue Joined..."); ///////////////////////
            }
        }
    }
    
    public void quitQueue(Player p) {
        synchronized (queue) {
            if (queue.contains(p)) {
                queue.remove(p);
                queue.notify();
                System.out.println("Queue Quited..."); ///////////////////////
            }
        }
    }
    
    public void stopQueue() {
        keepRunning = false;
    }
    
    public boolean isQueueWorking() {
        return keepRunning;
    }
}
