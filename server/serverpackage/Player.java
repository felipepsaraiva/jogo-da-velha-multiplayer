
package serverpackage;

import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.File;
import java.net.Socket;
import java.util.regex.Pattern;

public class Player extends Thread {
    //Connection Variables
    private FrmServer server;
    private Socket connection;
    private PrintWriter output;
    private BufferedReader input;
    private boolean allowConnection;
    //Player Variables
    private boolean loggedIn;
    private String login;
    private Database database;
    //private FileDatabase playerFile;
    //Game Variables
    private boolean playing;
    private Game game;
    private char mark;
    private Player opponent;
    
    //Constructor
    public Player(FrmServer server, Socket connection) {
        this.server = server;
        //Initializing Player Variables
        this.loggedIn = false;
        this.login = null;
        this.database = null;
        //Initializing Game Varialbes
        this.playing = false;
        this.game = null;
        this.mark = '~';
        this.opponent = null;
        //Initializing Conneciton Varialbes
        this.allowConnection = true;
        this.connection = connection;
        try {
            output = new PrintWriter(connection.getOutputStream(), true);
            input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        } catch (IOException ex) {
            System.out.println("IOException on Player Contructor: Player Died"); //////////////
        }
    }
    
    @Override
    public void run() {
        try {
            sendCommand("CONNECTED");
            String command;
            while (allowConnection) {
                if (playing) {
                    startGame();
                } else {
                    command = input.readLine();
                    commandHandler(command);
                }
            }
        } catch (IOException ex) {
            System.out.println("IOException on Player run method: Player Died");
            lostConnection();
        } finally {
            sendCommand("ENDING_CONNECTION");
            closeConnection();
        }
    }
    
    public void commandHandler(String command) {
        //JOIN_QUEUE
        if (command.equals("JOIN_QUEUE")) {
            if (loggedIn && login!=null && playing == false)
                server.getQueue().joinQueue(this);
        }
        
        //LOGIN <username> <password>
        else if (command.startsWith("LOGIN")) {
            String username = command.split(" ")[1];
            String pass = command.split(" ")[2];
            
            File playerFile = new File("Players\\" + username + ".txt");
            //Check if the file exists
            if (playerFile.exists()) {
                database = new Database(playerFile);
                //Check if the password is correct
                if (pass.equals(database.readData("Password"))) {
                    //Check if the player isn't already connected
                    if (!Boolean.parseBoolean(database.readData("Connected"))) {
                        login = database.readData("Login");
                        loggedIn = true;
                        database.setData("Connected", "true");
                        sendCommand("LOGIN_ACCEPTED");
                        sendCommand("INFO_GAMES " + database.readData("Victories") + " " + database.readData("Ties") + " " + database.readData("Defeats"));
                    }else
                        sendCommand("LOGIN_REFUSED This player is already connected!");
                } else
                    sendCommand("LOGIN_REFUSED Incorrect Password!");
            } else
                sendCommand("LOGIN_REFUSED This unsername doesn't exist!");
        }
        
        //SIGNUP <username> <password>
        else if (command.startsWith("SIGNUP")) {
            System.out.println("Signup request..."); //////////////////////
            String username = command.split(" ")[1];
            String pass = command.split(" ")[2];
            
            File playerFile = new File("Players\\" + username + ".txt");
            //Check if the file doesn't exist
            if (!playerFile.exists()) {
                boolean valid = true;
                //Check if all username characters matches with '\w' regex
                int length = username.length();
                for (int i=0 ; i<length ; i++)
                    if (!Pattern.matches("\\w", username.subSequence(i, i+1)))
                        valid = false;
                //Check if all username characters matches with '\w' regex
                length = pass.length();
                for (int i=0 ; i<length ; i++)
                    if (!Pattern.matches("\\w", pass.subSequence(i, i+1)))
                        valid = false;
                //Check if both username and password have valid characters
                if (valid) {
                    database = new Database(playerFile);
                    if (database.createFile(username, pass)) {
                        login = username;
                        loggedIn = true;
                        sendCommand("SIGNUP_ACCEPTED");
                        System.out.println("Signup accepted!"); //////////////////////
                    } else
                        sendCommand("SIGNUP_REFUSED Error creating database file... Sorry!");
                } else
                    sendCommand("SIGNUP_REFUSED Only letters and numbers are allowed!");
            } else
                sendCommand("SIGNUP_REFUSED This account already exists!");
        }
        
        //GET_INFO
        else if (command.startsWith("GET_INFO")) {
            if (loggedIn && login!=null) {
                //Enviar informações do player
            }
        }
        
    }
    
    public void setupGame(Game game, Player p, char mark) {
        this.game = game;
        this.opponent = p;
        this.mark = mark;
        this.playing = true;
        sendCommand("STARTING_GAME " + mark);
        System.out.println("Finished Setting the Game UP!"); ////////////////////
    }
    
    
    public void startGame() throws IOException {
        System.out.println("Starting Game..."); //////////////////
        String command;
        if (mark == 'X')
            sendCommand("MESSAGE Your Turn!");
        while (true) {
            command = input.readLine();
            //MOVE <n>
            if (command.startsWith("MOVE")) {
                int position = Integer.parseInt(command.substring(5));
                if (game.checkMove(position, this)) {
                    sendCommand("VALID_MOVE");
                    if (game.hasWinner()) {
                        addGamePoint("Victories");
                        sendCommand("VICTORY");
                    } else {
                        if (game.boardIfFull()) {
                            addGamePoint("Ties");
                            sendCommand("TIE");
                        }
                    }
                        
                } else
                    sendCommand("MESSAGE Posição Inválida!");
            }
            //QUIT_GAME
            else if (command.startsWith("QUIT_GAME")) {
                if (!game.hasFinished()) {
                    game.finish();
                    playing = false; //Essa linha garante que o player não esteja jogando quando o oponente checar
                    opponent.otherPlayerQuited();
                    addGamePoint("Defeats");
                }
                break;
            }
        }
        //Ending game
        game = null;
        opponent = null;
        mark = '~';
        playing = false;
        System.out.println("Quiting Game..."); ////////////////////
        sendCommand("INFO_GAMES " + database.readData("Victories") + " " + database.readData("Ties") + " " + database.readData("Defeats"));
    }
    
    public void otherPlayerMoved(int position) {
        sendCommand("OPPONENT_MOVED " + position);
        if (game.hasWinner()) {
            addGamePoint("Defeats");
            sendCommand("DEFEAT");
        } else if (game.boardIfFull()) {
            addGamePoint("Ties");
            sendCommand("TIE");
            
        }
    }
    
    public void otherPlayerQuited() {
        if (playing && !opponent.isPlaying()) {
            addGamePoint("Victories");
            sendCommand("OPPONENT_QUITED");
        }
    }
    
    private void sendCommand(String command) {
        synchronized (output) {
            output.println(command);
        }
    }
    
    public void lostConnection() {
        if (loggedIn) {
            if (playing) {
                if (!game.hasFinished()) {
                    game.finish();
                    playing = false; //Essa linha garante que o player não esteja jogando quando o oponente checar
                    opponent.otherPlayerQuited();
                }
            } else {
                server.getQueue().quitQueue(this);
            }
            database.setData("Connected", "false");
        }
    }
    
    public void closeConnection() {
        try {
            input.close();
            output.close();
            connection.close();
        } catch (IOException ex) {}
    }
    
    public boolean isPlaying() {
        return playing;
    }
    
    public Player getOpponent() {
        if (playing)
            return this.opponent;
        else
            return null;
    }
    
    public char getMark() {
        return mark;
    }
    
    private void addGamePoint(String category) {
        if (loggedIn && database != null) {
            int score = Integer.parseInt(database.readData(category).trim());
            score++;
            database.setData(category, String.valueOf(score));
        }
    }
    
    public void setAllowConnection(boolean value) {
        allowConnection = value;
    }
}
