
package serverpackage;

public class Game {
    private boolean finished;
    private Player currentPlayer;
    private char[] board = {
        '-', '-', '-',          // 0  1  2
        '-', '-', '-',          // 3  4  5
        '-', '-', '-'  };       // 6  7  8
    
    public Game(Player p) {
        this.currentPlayer = p;
    }
    
    //Checa se existe um ganhador. Se existir, altera o atributo finished
    public boolean hasWinner() {
        /*if (board[0] != '-') {
            //Checking Row 0
            if (board[0] == board[1] && board[0] == board[2])
                return true;
            //Checking Column 0
            else if (board [0] == board[3] && board[0] == board[6])
                return true;
        } else if (board[4] != '-') {
            //Checking Row 1
            if (board[4] == board[3] && board[4] == board[5])
                return true;
            //Checking Column 1
            else if (board [4] == board[1] && board[4] == board[7])
                return true;
            //Checking Main Diagonal
            else if (board[4] == board [0] && board[4] == board[8])
                return true;
            //Checking Secondary Diagonal
            else if (board[4] == board [2] && board[4] == board[6])
                return true;
        } else if (board[8] != '-') {
            //Checking Row 2
            if (board[8] == board[6] && board[8] == board[7])
                return true;
            //Checking Column 2
            else if (board [8] == board[2] && board[8] == board[5])
                return true;
        }
        return false;*/
        
        if ((board[0] != '-' && board[0] == board[1] && board[0] == board[2])
          ||(board[3] != '-' && board[3] == board[4] && board[3] == board[5])
          ||(board[6] != '-' && board[6] == board[7] && board[6] == board[8])
          ||(board[0] != '-' && board[0] == board[3] && board[0] == board[6])
          ||(board[1] != '-' && board[1] == board[4] && board[1] == board[7])
          ||(board[2] != '-' && board[2] == board[5] && board[2] == board[8])
          ||(board[0] != '-' && board[0] == board[4] && board[0] == board[8])
          ||(board[2] != '-' && board[2] == board[4] && board[2] == board[6])) {
            finished = true;
            return true;
        }
        return false;
    }
    
    //Checa se o tabuleiro está cheio. Se estiver, altera o atributo finished
    public boolean boardIfFull() {
        for (int i=0 ; i<9 ; i++) {
            if (board[i] == '-')
                return false;
        }
        finished = true;
        return true;
    }
    
    /* Checa se o player que invocou a função é o player atual e se o movimento é valido.
       Se for, atualiza o tabuleiro, troca o player atual,
       e avisa o novo player atual que seu oponente realizou uma jogada */
    public synchronized boolean checkMove(int position, Player p) {
        if (p == currentPlayer && board[position] == '-') {
            board[position] = p.getMark();
            currentPlayer = p.getOpponent();
            currentPlayer.otherPlayerMoved(position);
            return true;
        } else
            return false;
    }
    
    public boolean hasFinished() {
        return finished;
    }
    
    public void finish() {
        finished = true;
    }
}
