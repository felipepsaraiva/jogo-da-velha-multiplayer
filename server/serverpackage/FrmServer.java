
package serverpackage;

//import java.io.File;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class FrmServer extends javax.swing.JFrame {
    
    public FrmServer(int port) {
        initComponents();
        this.port = port;
        keepRunning = true;
        players = new ArrayList<Player>();
        //Creates Players directory if it doesn't exist
        File playersDir = new File("Players");
        if (!playersDir.exists()) {
            try {
                playersDir.mkdir();
            } catch (Exception ex) {
                System.out.println("Error creating Players dir...");
                ex.printStackTrace();
            }
        }
            
            
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
    
    //Connection Variables
    private ServerSocket server;
    private int port;
    private boolean keepRunning;
    //Machanisms Variables
    private GameQueue queue;
    private ArrayList<Player> players;
    
    public void runServer() {
        System.out.println("RunningServer..."); ////////////////
        queue = new GameQueue(this);
        new Thread(queue).start();
        Player p;
        try {
            server = new ServerSocket(port);
            while (keepRunning) {
                try {
                    p = new Player(this, server.accept());
                    System.out.println("Connection Accepted!"); ////////////
                    players.add(p);
                    p.start();
                    //Refresh Players List
                } catch (IOException ex) {
                    System.out.println("Erro aceitando conexão!");
                    ex.printStackTrace();
                }
            }
        } catch (IOException ex) {
            System.out.println("Erro abrindo o server!");
            ex.printStackTrace();
            System.exit(1);
        } finally {
            try {
                server.close();
            } catch (IOException ex) {}
        }
    }
    
    public void updateTable(String login, String ip, String status) {
        //If String equals null, it should keep the same thing there
    }
    
    public GameQueue getQueue() {
        return queue;
    }
    
}
