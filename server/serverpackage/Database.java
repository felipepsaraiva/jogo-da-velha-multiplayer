
package serverpackage;

import java.util.Formatter;
import java.io.*;

public class Database {
    private String fileName;
    private File file;
    
    public Database(String path) {
        this.file = new File(path);
        this.fileName = file.getName().replace(".txt", "");
    }
    
    public Database(File file) {
        this.file = file;
        this.fileName = file.getName().replace(".txt", "");
    }
    
    public boolean createFile(String login, String pass) {
        if (!file.exists()) {
            Formatter fileCreator = null;
            boolean created = false;
            try {
                fileCreator = new Formatter(file);
                fileCreator.format("Connected: true"
                        + "\nUsername: " + login
                        + "\nPassword: " + pass
                        + "\nVictories: 0"
                        + "\nTies: 0"
                        + "\nDefeats: 0");
                created = true;
            } catch (IOException ex) {
                System.err.println("Erro ao criar arquivo!");
                created = false;
            } finally {
                if (fileCreator != null)
                    fileCreator.close();
            }
            return created;
        } else
            return false;
    }
    
    public String readData(String parameter) {
        BufferedReader fileReader = null;
        String output = null;
        try {
            fileReader = new BufferedReader( new FileReader(file) );
            output = fileReader.readLine();
            while (output != null) {
                if (output.startsWith(parameter + ":")) {
                    output = (output.split(parameter + ":")[1].trim());
                    break;
                }
                output = fileReader.readLine();
            }
            if (output == null)
                output = "Invalid!";
        } catch (FileNotFoundException e) {
            System.err.println("FileNotFoundException: " + e.getMessage());
        } catch (IOException e1) {
            System.err.println("IOException: " + e1.getMessage());
        } finally {
            closeFile(fileReader);
        }
        return output;
    }
    
    public boolean setData(String parameter, String data) {
        boolean done = true;
        BufferedReader fileReader = null;
        Formatter fileFormatter = null;
        File tempFile = new File(fileName + "-temp.txt");
        //Trata se o arquivo temporário já existe
        int fileIndex = 0;
        while (tempFile.exists()) {
            tempFile = new File(fileName + "-temp" + fileIndex + ".txt");
            fileIndex++;
        }
        try {
            fileReader = new BufferedReader(new FileReader(file));
            fileFormatter = new Formatter(tempFile);
            String line = fileReader.readLine();
            boolean parameterExists = false;
            boolean firstLine = true;
            while (line != null) {
                if (line.startsWith(parameter + ":")) {
                    if (firstLine)
                        fileFormatter.format(parameter + ": " + data);
                    else
                        fileFormatter.format("\n" + parameter + ": " + data);
                    parameterExists = true;
                } else {
                    if (firstLine)
                        fileFormatter.format(line);
                    else
                        fileFormatter.format("\n" + line);
                }
                fileFormatter.flush();
                line = fileReader.readLine();
                firstLine = false;
            }
            if (!parameterExists)
                fileFormatter.format("\n" + parameter + ": " + data);
        } catch (FileNotFoundException e1) {
            System.err.println("FileNotFoundException: " + e1.getMessage());
            done = false;
        } catch (IOException e) {
            System.err.println("IOException: " + e.getMessage());
            done = false;
        } finally {
            closeFile(fileReader);
            closeFile(fileFormatter);
        }
        if (done) {
            file.delete();
            tempFile.renameTo(file);
        } else
            tempFile.delete();
        return done;
    }
    
    private void closeFile(Reader file) {
        try {
            file.close();
        } catch (IOException e1) {
            System.err.println("IOException (Not Important): " + e1.getMessage());
        } catch (NullPointerException e2) {}
    }
    
    private void closeFile(Formatter file) {
        try {
            file.close();
        } catch (NullPointerException e2) {}
    }
    
    public boolean fileExists() {
        return (file.exists());
    }
    
}