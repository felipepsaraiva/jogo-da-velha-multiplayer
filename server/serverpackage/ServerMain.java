
package serverpackage;

public class ServerMain {
    
    public static void main(String[] args) {
        FrmServer serverApp = new FrmServer(6789);
        serverApp.setVisible(true);
        serverApp.runServer();
    }
    
}
