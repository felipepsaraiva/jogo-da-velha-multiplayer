
package clientpackage;

public class ClientMain {
    
    public static void main(String[] args) {
        FrmClient clientApp = new FrmClient("localhost", 6789);
        clientApp.setVisible(true);
        clientApp.runClient();
    }
    
}
