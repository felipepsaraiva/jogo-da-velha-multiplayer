
package clientpackage;

import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import javax.swing.JOptionPane;
import javax.swing.JFrame;

public class FrmClient extends JFrame {
    //Constants
    private final int MAX_CONNECTIONS = 15;
    private final long BETWEEN_CONNECTIONS_TIME = 5000;
    //Connection Variables
    private InetSocketAddress ipSocketAddress;
    private Socket connection;
    private PrintWriter output;
    private BufferedReader input;
    private String hostIP;
    private int port;
    private boolean connected;
    //GUI Varialbles
    private GamePanel gamePanel;
    private LoginPanel loginPanel;
    private InfosPanel infosPanel;
    //Other Variables
    private boolean playing;

    public FrmClient(String ip, int port) {
        super ("Jogo Da Velha - Client");
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        this.hostIP = ip;
        this.port = port;
        this.playing = false;
        ipSocketAddress = new InetSocketAddress(ip, port);
        connection = new Socket();

        loginPanel = new LoginPanel(this);
        gamePanel = new GamePanel(this);
        infosPanel = new InfosPanel(this);
        add(loginPanel);
        pack();
    }

    public void runClient() {
        while (true) {
            try {
                waitForConnection();
                setupStreams();
                whileCommunicating();
            } catch (EOFException ex1) {
                ex1.printStackTrace();
            } catch (UnknownHostException ex2) {
                JOptionPane.showMessageDialog(this, "Host Desconhecido!");
                break;
            } catch (IOException ex) {
                //Existe uma solução melhor?
                if (ex.getMessage().equals("Server Offline!"))
                    break;
                else
                    System.out.println("Reset Everything and try to reconnect...");
            } finally {
                connected = false;
                closeConnection();
            }
        }
    }

    private void waitForConnection() throws IOException, UnknownHostException {
        System.out.println("Waiting for Connection...");//////////////
        boolean succeed = false;
        for (int i=0 ; i<MAX_CONNECTIONS && !connected ; i++) {
            System.out.println("Connecting..."); ////////////////////
            try {
                connection.connect(ipSocketAddress, 1000);
                succeed = true;
                break;
            } catch (IOException ex) {
                succeed = false;
                try {
                    Thread.sleep(BETWEEN_CONNECTIONS_TIME);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            }
        }

        if (!succeed) {
            loginPanel.setStatus(-1);
            throw new IOException ("Server Offline!");
        }
        System.out.println("Connection established!");/////////////////////
        //System.gc();
    }

    private void setupStreams() throws IOException {
        output = new PrintWriter(connection.getOutputStream(), true);
        output.flush();
        input = new BufferedReader( new InputStreamReader(connection.getInputStream()) );
        System.out.println("Streams are ok!");///////////////////
    }

    private void whileCommunicating() throws IOException {
        String command;
        while (true) {
            command = input.readLine();
            if (playing)
                gamePanel.inGameCommandHandler(command);
            else
                commandHandler(command);
        }
    }

    private void commandHandler(String command) {
        //STARTING_GAME <mark>
        if (command.startsWith("STARTING_GAME")) {
            playing = true;
            gamePanel.initiateGame(command.charAt(14));
            changePanel("Game");
            //Stop The Waiting for MenuCommands
            sendCommand("");
        }
        //INFO_GAMES <n: victories> <n: ties> <n: defeats>
        else if (command.startsWith("INFO_GAMES")) {
            String[] split = command.split(" ");
            infosPanel.updateGamesInfo(split[1], split[2], split[3]);
        }
        //LOGIN_ACCEPTED
        else if (command.startsWith("LOGIN_ACCEPTED")) {
            changePanel("Infos");
        }
        //LOGIN_REFUSED <reason>
        else if (command.startsWith("LOGIN_REFUSED")){
            String[] split = command.split(" ");
            String reason = split[1];
            for (int i=2 ; i<split.length ; i++)
                reason += (" " + split[i]);
            loginPanel.setRefuseMessage(reason);
        }
        //SIGNUP_ACCEPTED
        else if (command.startsWith("SIGNUP_ACCEPTED")) {
            JOptionPane.showMessageDialog(this, "Your account has been created!");
            changePanel("Infos");
        }
        //SIGNUP_REFUSED <reason>
        else if (command.startsWith("SIGNUP_REFUSED")) {
            String[] split = command.split(" ");
            String reason = split[1];
            for (int i=2 ; i<split.length ; i++)
                reason += (" " + split[i]);
            loginPanel.setRefuseMessage(reason);
        }
        //CONNECTED
        else if (command.startsWith("CONNECTED")) {
            connected = true;
            loginPanel.setStatus(1);
        }
        //ENDING_CONNECTION
        else if (command.startsWith("ENDING_CONNECTION")) {
            System.out.println("Bye!");
        }
    }

    protected void sendCommand(String command) {
        if (connected)
            synchronized (output) {
                if (connection != null) {
                    output.println(command);
                }
            }
    }

    protected void changePanel (String panelName) {
        remove(loginPanel);
        remove(gamePanel);
        remove(infosPanel);

        //Size +16x + 38y
        if (panelName.equals("Game")) {
            add(gamePanel);
        } else if (panelName.equals("Infos")) {
            add(infosPanel);
            playing = false;
        } else if (panelName.equals("Login")) {
            add(loginPanel);
        }
        pack();
    }

    public void closeConnection() {
        try {
            output.close();
            input.close();
            connection.close();
        } catch (IOException | NullPointerException ex) {}
    }

    public boolean isConnected() {
        return connected;
    }

}
