
package clientpackage;

import java.io.IOException;
import java.awt.Graphics;
import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Dimension;

public class GamePanel extends javax.swing.JPanel {

    public GamePanel(FrmClient client) {
        initComponents();
        this.setPreferredSize(new Dimension(320, 403));
        this.client = client;
        lblPosition = new JLabel[]{
                lblPosition0, lblPosition1, lblPosition2,
                lblPosition3, lblPosition4, lblPosition5,
                lblPosition6, lblPosition7, lblPosition8};
        myMove = -1;
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        try {
            g.drawImage(ImageIO.read(getClass().getResource("/clientpackage/Tabuleiro.png")), 10, 60, null);
        } catch (IOException ex) {}
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblText = new javax.swing.JLabel();
        lblPosition0 = new javax.swing.JLabel();
        lblPosition1 = new javax.swing.JLabel();
        lblPosition2 = new javax.swing.JLabel();
        lblPosition3 = new javax.swing.JLabel();
        lblPosition4 = new javax.swing.JLabel();
        lblPosition5 = new javax.swing.JLabel();
        lblPosition6 = new javax.swing.JLabel();
        lblPosition7 = new javax.swing.JLabel();
        lblPosition8 = new javax.swing.JLabel();
        cmdQuit = new javax.swing.JButton();

        setLayout(null);

        lblText.setFont(new java.awt.Font("Tekton Pro", 0, 24)); // NOI18N
        lblText.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblText.setText("Espere o Oponente...");
        add(lblText);
        lblText.setBounds(10, 10, 300, 39);

        lblPosition0.setFont(new java.awt.Font("Comic Sans MS", 0, 60)); // NOI18N
        lblPosition0.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPosition0.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblPosition0MouseClicked(evt);
            }
        });
        add(lblPosition0);
        lblPosition0.setBounds(10, 60, 96, 96);

        lblPosition1.setFont(new java.awt.Font("Comic Sans MS", 0, 60)); // NOI18N
        lblPosition1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPosition1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblPosition1MouseClicked(evt);
            }
        });
        add(lblPosition1);
        lblPosition1.setBounds(112, 60, 96, 96);

        lblPosition2.setFont(new java.awt.Font("Comic Sans MS", 0, 60)); // NOI18N
        lblPosition2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPosition2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblPosition2MouseClicked(evt);
            }
        });
        add(lblPosition2);
        lblPosition2.setBounds(214, 60, 96, 96);

        lblPosition3.setFont(new java.awt.Font("Comic Sans MS", 0, 60)); // NOI18N
        lblPosition3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPosition3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblPosition3MouseClicked(evt);
            }
        });
        add(lblPosition3);
        lblPosition3.setBounds(10, 162, 96, 96);

        lblPosition4.setFont(new java.awt.Font("Comic Sans MS", 0, 60)); // NOI18N
        lblPosition4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPosition4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblPosition4MouseClicked(evt);
            }
        });
        add(lblPosition4);
        lblPosition4.setBounds(112, 162, 96, 96);

        lblPosition5.setFont(new java.awt.Font("Comic Sans MS", 0, 60)); // NOI18N
        lblPosition5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPosition5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblPosition5MouseClicked(evt);
            }
        });
        add(lblPosition5);
        lblPosition5.setBounds(214, 162, 96, 96);

        lblPosition6.setFont(new java.awt.Font("Comic Sans MS", 0, 60)); // NOI18N
        lblPosition6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPosition6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblPosition6MouseClicked(evt);
            }
        });
        add(lblPosition6);
        lblPosition6.setBounds(10, 264, 96, 96);

        lblPosition7.setFont(new java.awt.Font("Comic Sans MS", 0, 60)); // NOI18N
        lblPosition7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPosition7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblPosition7MouseClicked(evt);
            }
        });
        add(lblPosition7);
        lblPosition7.setBounds(112, 264, 96, 96);

        lblPosition8.setFont(new java.awt.Font("Comic Sans MS", 0, 60)); // NOI18N
        lblPosition8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPosition8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblPosition8MouseClicked(evt);
            }
        });
        add(lblPosition8);
        lblPosition8.setBounds(214, 264, 96, 96);

        cmdQuit.setText("Sair");
        cmdQuit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                finalizeGame(evt);
            }
        });
        add(cmdQuit);
        cmdQuit.setBounds(235, 370, 75, 23);
    }// </editor-fold>//GEN-END:initComponents

    private void finalizeGame(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_finalizeGame
        finalizeGame();
    }//GEN-LAST:event_finalizeGame

    private void lblPosition0MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPosition0MouseClicked
        move(0);
    }//GEN-LAST:event_lblPosition0MouseClicked

    private void lblPosition1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPosition1MouseClicked
        move(1);
    }//GEN-LAST:event_lblPosition1MouseClicked

    private void lblPosition2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPosition2MouseClicked
        move(2);
    }//GEN-LAST:event_lblPosition2MouseClicked

    private void lblPosition3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPosition3MouseClicked
        move(3);
    }//GEN-LAST:event_lblPosition3MouseClicked

    private void lblPosition4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPosition4MouseClicked
        move(4);
    }//GEN-LAST:event_lblPosition4MouseClicked

    private void lblPosition5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPosition5MouseClicked
        move(5);
    }//GEN-LAST:event_lblPosition5MouseClicked

    private void lblPosition6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPosition6MouseClicked
        move(6);
    }//GEN-LAST:event_lblPosition6MouseClicked

    private void lblPosition7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPosition7MouseClicked
        move(7);
    }//GEN-LAST:event_lblPosition7MouseClicked

    private void lblPosition8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPosition8MouseClicked
        move(8);
    }//GEN-LAST:event_lblPosition8MouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cmdQuit;
    private javax.swing.JLabel lblPosition0;
    private javax.swing.JLabel lblPosition1;
    private javax.swing.JLabel lblPosition2;
    private javax.swing.JLabel lblPosition3;
    private javax.swing.JLabel lblPosition4;
    private javax.swing.JLabel lblPosition5;
    private javax.swing.JLabel lblPosition6;
    private javax.swing.JLabel lblPosition7;
    private javax.swing.JLabel lblPosition8;
    private javax.swing.JLabel lblText;
    // End of variables declaration//GEN-END:variables
    
    //Other Variables
    private FrmClient client;
    private JLabel[] lblPosition;
    private char mark;
    private int myMove;
    
    public void initiateGame(char mark) {
        this.mark = mark;
    }
    
    private void finalizeGame() {
        if (cmdQuit.getText().equals("Sair")) {
            int response = JOptionPane.showConfirmDialog(client, 
                    "Tem certeza que deseja sair?", 
                    "Voce está em jogo!", 
                    JOptionPane.YES_NO_OPTION);
            if (response != JOptionPane.YES_OPTION)
                return;
        }
        for(int i=0 ; i<9 ; i++) {
            lblPosition[i].setEnabled(true);
            lblPosition[i].setText("");
        }
        lblText.setText("Espere o Oponente...");
        cmdQuit.setText("Sair");
        myMove = -1;
        mark = '~';
        client.changePanel("Infos");
        client.sendCommand("QUIT_GAME");
    }
    
    private void move(int position) {
        myMove = position;
        client.sendCommand("MOVE " + position);
    }
    
    public void inGameCommandHandler(String command) {
        //VALID_MOVE
        if (command.startsWith("VALID_MOVE")) {
            lblPosition[myMove].setText(String.valueOf(mark));
            lblText.setText("Espere o Oponente...");
        }
        //OTHER_PLAYER_MOVED
        else if (command.startsWith("OPPONENT_MOVED")) {
            int pos = Integer.parseInt(command.substring(15));
            if (mark == 'X')
                lblPosition[pos].setText("O");
            else
                lblPosition[pos].setText("X");
            lblText.setText("Sua Vez!");
        }
        //MESSAGE
        else if (command.startsWith("MESSAGE")) {
            lblText.setText(command.substring(8));
        }
        //VICTORY
        else if (command.startsWith("VICTORY")) {
            lblText.setText("Você Venceu!");
            disableLabels();
        }
        //DEFEAT
        else if (command.startsWith("DEFEAT")) {
            lblText.setText("Você Perdeu!");
            disableLabels();
        }
        //TIE
        else if (command.startsWith("TIE")) {
            lblText.setText("Velha!");
            disableLabels();
        }
        //OPPONENT_QUITED
        else if (command.startsWith("OPPONENT_QUITED")) {
            JOptionPane.showMessageDialog(client, "Seu oponente saiu da partida!");
            cmdQuit.setText("Voltar");
            finalizeGame();
        }
    }
    
    private void disableLabels() {
        //Disabling labels
        for(int i=0 ; i<9 ; i++)
            lblPosition[i].setEnabled(false);
        cmdQuit.setText("Voltar");
    }
}
