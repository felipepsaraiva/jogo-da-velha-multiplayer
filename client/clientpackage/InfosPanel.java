
package clientpackage;

public class InfosPanel extends javax.swing.JPanel {

    public InfosPanel(FrmClient client) {
        initComponents();
        this.frmClient = client;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cmdJoinQueue = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        lblVictories = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lblTies = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        lblDefeats = new javax.swing.JLabel();

        cmdJoinQueue.setText("Join Queue");
        cmdJoinQueue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdJoinQueueActionPerformed(evt);
            }
        });

        jLabel1.setText("Victories:");

        lblVictories.setText("0");

        jLabel2.setText("Ties:");

        lblTies.setText("0");

        jLabel3.setText("Defeats:");

        lblDefeats.setText("0");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(152, 152, 152)
                        .addComponent(cmdJoinQueue))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addGap(35, 35, 35)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblDefeats)
                            .addComponent(lblTies)
                            .addComponent(lblVictories))))
                .addContainerGap(161, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(78, 78, 78)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(lblVictories))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(lblTies))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(lblDefeats))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 72, Short.MAX_VALUE)
                .addComponent(cmdJoinQueue)
                .addGap(63, 63, 63))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void cmdJoinQueueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdJoinQueueActionPerformed
        frmClient.sendCommand("JOIN_QUEUE");
    }//GEN-LAST:event_cmdJoinQueueActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cmdJoinQueue;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel lblDefeats;
    private javax.swing.JLabel lblTies;
    private javax.swing.JLabel lblVictories;
    // End of variables declaration//GEN-END:variables
    
    //Other Variables
    private FrmClient frmClient;
    
    public void updateGamesInfo(String v, String t, String d) {
        if (!v.equals("-1"))
            lblVictories.setText(v);
        if (!t.equals("-1"))
            lblTies.setText(t);
        if (!d.equals("-1"))
            lblDefeats.setText(d);
    }
}
